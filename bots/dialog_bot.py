# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

import time
from datetime import datetime
import json
from botbuilder.core import ActivityHandler, ConversationState, UserState, TurnContext, StoreItem, MemoryStorage
from botbuilder.dialogs import Dialog
from helpers.dialog_helper import DialogHelper
from botbuilder.azure import CosmosDbPartitionedStorage, CosmosDbPartitionedConfig, BlobStorage, BlobStorageSettings
from services import deletefile, readfile, jsonlog_init, jsonlog_read, jsonlog_write

from config import DefaultConfig

from services import Luis

from data_models import ConversationData, UserProfile

class UtteranceLog(StoreItem):
    """
    Class for storing a log of utterances (text of messages) as a list.
    """

    def __init__(self):
        super(UtteranceLog, self).__init__()
        self.utterance_list = []
        self.turn_number = 0
        self.e_tag = "*"


class DialogBot(ActivityHandler):
    def __init__(
        self,
        conversation_state: ConversationState,
        user_state: UserState,
        dialog: Dialog,
        config: DefaultConfig
    ):
        if conversation_state is None:
            raise Exception(
                "[DialogBot]: Missing parameter. conversation_state is required"
            )
        if user_state is None:
            raise Exception("[DialogBot]: Missing parameter. user_state is required")
        if dialog is None:
            raise Exception("[DialogBot]: Missing parameter. dialog is required")

        self.conversation_state = conversation_state
        self.user_state = user_state
        self.dialog = dialog
        self.config = DefaultConfig()
        #self.storage = MemoryStorage()
        self.conversation_data_accessor = self.conversation_state.create_property(
            "ConversationData"
        )
        self.user_profile_accessor = self.user_state.create_property("UserProfile")


        cosmos_config = CosmosDbPartitionedConfig(
            cosmos_db_endpoint=self.config.COSMOS_DB_ENDPOINT,
            auth_key=self.config.COSMOS_DB_AUTH_KEY,
            database_id=self.config.COSMOS_DB_DATABASE_ID,
            container_id = self.config.COSMOS_DB_DATABASE_ID,
            compatibility_mode = False
        )
        self.storage = CosmosDbPartitionedStorage(cosmos_config)

    async def on_turn(self, turn_context: TurnContext):
        if turn_context.activity.type == 'message':
            if turn_context.activity.text == None and turn_context.activity.value != None:
                turn_context.activity.text = json.dumps(turn_context.activity.value) 

        await super().on_turn(turn_context)

        # Save any state changes that might have occurred during the turn.
        await self.conversation_state.save_changes(turn_context, False)
        await self.user_state.save_changes(turn_context, False)

        id = readfile(file="data/json/tmp/tmp.txt")
        x = readfile(file=f"data/json/tmp/{id}.json")
        data = {id: json.loads(x)}

        await self.storage.write(data)

    async def on_message_activity(self, turn_context: TurnContext):

            # Get the state properties from the turn context.
        #user_profile = await self.user_profile_accessor.get(turn_context, UserProfile)

        # message = turn_context.activity.text
        # try:
        #     intent = Luis(query=message)['topIntent']
        #     if intent == "Choice.Cancel":
        #         turn_context.begin_dialog(MainDialog.__name__)
        #         print("cancel")
        # except:
        #     print("exception")

        d = jsonlog_read()
        d['timestamp2'] = str(datetime.utcnow())
        d['ConversationTime'] = str(datetime.utcnow() - datetime.strptime(d['timestamp'], '%Y-%m-%d %H:%M:%S.%f'))
        jsonlog_write(d)


        # conversation_data = await self.conversation_data_accessor.get(
        #     turn_context, ConversationData
        # )

        # conversation_data.timestamp = self.__datetime_from_utc_to_local(
        #         turn_context.activity.timestamp
        #     )
        # conversation_data.channel_id = turn_context.activity.channel_id

        # # read the state object
        # store_items = await self.storage.read(["UtteranceLog"])

        await DialogHelper.run_dialog(
            self.dialog,
            turn_context,
            self.conversation_state.create_property("DialogState"),
        )

    def __datetime_from_utc_to_local(self, utc_datetime):
        now_timestamp = time.time()
        offset = datetime.fromtimestamp(now_timestamp) - datetime.utcfromtimestamp(
            now_timestamp
        )
        result = utc_datetime + offset
        return result.strftime("%I:%M:%S %p, %A, %B %d of %Y")
