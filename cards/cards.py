import os
from botbuilder.core import MessageFactory, CardFactory
from botbuilder.schema import ChannelAccount, CardAction, ActionTypes, SuggestedActions, Attachment, Activity, ActivityTypes
import json


def displayCard(card):
    with open(f"cards/{card}.json") as jsonfile:
        data = json.load(jsonfile)
    
    return Activity(
                    #text=f"You selected {selection}",
                    type=ActivityTypes.message,
                    attachments=[CardFactory.adaptive_card(data)],
                )

def suggested(text, actions):
    reply = MessageFactory.text(text)
    reply.suggested_actions = SuggestedActions(
        actions=[CardAction(title=i, type=ActionTypes.im_back, value=i) for i in actions]
    )
    return reply