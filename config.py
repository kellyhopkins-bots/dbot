#!/usr/bin/env python3
# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

import os

""" Bot Configuration """


class DefaultConfig:
    """ Bot Configuration """

    PORT = 3978
    APP_ID = os.environ.get("MicrosoftAppId", "")
    APP_PASSWORD = os.environ.get("MicrosoftAppPassword", "")
    QNA_KNOWLEDGEBASE_ID = os.environ.get("QnAKnowledgeBaseID", "3be28a92-95ff-4971-bb04-5dd930c5f913")
    # QNA_ENDPOINT_HOST = "https://mbda-qna-bot.azurewebsites.net/qnamaker" #os.environ.get("QnAEndpoint", "")
    QNA_ENDPOINT_HOST = os.environ.get("QnAEndpoint", "https://mbda-qna.azurewebsites.net/qnamaker")
    QNA_ENDPOINT_KEY = os.environ.get("QNAEndpointKey", "f55cddc4-79fc-483d-80e0-ae7869c416db")

    LUIS_APP_ID = os.environ.get("LuisAppId", "047ec65f-1de3-4b9c-b256-ccf0af193624")
    LUIS_API_KEY = os.environ.get("LuisAPIKey", "6aa95565f22846febe4fa49366fe5576")
    # LUIS endpoint host name, ie "westus.api.cognitive.microsoft.com"
    LUIS_API_HOST_NAME =  os.environ.get("LuisAPIHostName", "westus.api.cognitive.microsoft.com")

    COSMOS_DB_ENDPOINT = "https://commerce-chatbot-data.documents.azure.com:443/"
    COSMOS_DB_AUTH_KEY="1lB0BPyV6OkgBK3iyjj0zLdKpLRiP2NUsvLuFYYLW6BrC9EdhATPGLy192z4lg2OGPJQf3J1I6n6KLiMS7tz1A=="
    COSMOS_DB_DATABASE_ID="chatbot-data"
    COSMOS_DB_CONTAINER_ID="chatbot-data"

    password = "HpP9TmLDfSrB8Smj"
