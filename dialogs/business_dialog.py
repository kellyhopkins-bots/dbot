from botbuilder.core import MessageFactory, CardFactory
from botbuilder.dialogs import (
    WaterfallDialog,
    DialogTurnResult,
    WaterfallStepContext,
    ComponentDialog,
)
from botbuilder.dialogs.prompts import PromptOptions, TextPrompt, NumberPrompt, ChoicePrompt
from botbuilder.dialogs.choices import Choice, ListStyle
from botbuilder.schema import ChannelAccount, CardAction, ActionTypes, SuggestedActions, Attachment, Activity, ActivityTypes

from data_models import UserProfile
from dialogs.info_dialog import InfoDialog

from services import Qna, Luis, bclookup, bcinfo, ziplookup, crank, jsonlog_read, jsonlog_write
from cards import cards

import json

CARD_PROMPT = "cardPrompt"

class BusinessDialog(ComponentDialog):
    def __init__(self, dialog_id: str = None):
        super(BusinessDialog, self).__init__(dialog_id or BusinessDialog.__name__)

        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(NumberPrompt(NumberPrompt.__name__))
        self.add_dialog(ChoicePrompt(CARD_PROMPT))

        self.add_dialog(
            WaterfallDialog(
                "WFDialog",
                [
                    self.first_step, self.second_step, self.third_step, self.fourth_step, self.fifth_step
                ]
            )
        )

        self.initial_dialog_id = "WFDialog"
    
    async def first_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        
        return await step_context.prompt(
            CARD_PROMPT,
            PromptOptions(
                prompt=MessageFactory.text("I can help you locate a business center based on its location or domain specialization (e.g., manufacturing or export). How would you like to search?"),
                retry_prompt=MessageFactory.text("Select a valid option"),
                choices = [
                    Choice(value="Location"),
                    Choice(value="Domain"),
                ]
            )
        )
    
    async def second_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        selection = step_context.result.value

        if selection == "Domain":
            return await step_context.prompt(
                CARD_PROMPT,
                PromptOptions(
                    prompt=MessageFactory.text("Choose a domain area:"),
                    retry_prompt=MessageFactory.text("Select a valid option"),
                    choices= [
                        Choice(value="Export Centers", synonyms=['Exp', 'Export']),
                        Choice(value="Manufacturing Centers"),
                        Choice(value="General Centers")
                    ],
                    style= ListStyle("4")
                )
            )
        elif selection == "Location":
            return await step_context.prompt(
                TextPrompt.__name__,
                PromptOptions(
                    prompt=MessageFactory.text("Enter your zipcode:")
                )
            )
    
    async def third_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        try:
            selection = step_context.result.value

            d = jsonlog_read()
            d['BusinessCenterDomainSearch'].append(selection)
            jsonlog_write(d)

        except:
            selection = step_context.result

            d = jsonlog_read()
            d['BusinessCenterZipSearch'].append(selection)
            jsonlog_write(d)

        if selection == "Manufacturing Centers":
            return await step_context.prompt(
                CARD_PROMPT,
                PromptOptions(
                    prompt=MessageFactory.text("Choose a business center to learn more"),
                    retry_prompt=MessageFactory.text("Select a valid option"),
                    choices= [Choice(value=i) for i in bclookup("Manufacturing")],
                    style= ListStyle("4")
                )
            )
        elif selection == "Export Centers":
            options = ["Africa Regional Expertise", "Asia Regional Expertise", "Latin America Regional Expertise", "Caribbean Expertise"]
            return await step_context.prompt(
                CARD_PROMPT,
                PromptOptions(
                    prompt=MessageFactory.text("Choose a business center to learn more"),
                    retry_prompt=MessageFactory.text("Select a valid option"),
                    # choices= [Choice(value=i) for i in bclookup("Exporting")],
                    choices= [Choice(value=i) for i in options],
                    style= ListStyle("4")
                )
            )
        elif selection == "General Centers":
            return await step_context.prompt(
                CARD_PROMPT,
                PromptOptions(
                    prompt=MessageFactory.text("Choose a business center to learn more"),
                    retry_prompt=MessageFactory.text("Select a valid option"),
                    choices= [Choice(value=i) for i in bclookup("General")],
                    style= ListStyle("5")
                )
            )
        else:
            return await step_context.prompt(
                CARD_PROMPT,
                PromptOptions(
                    prompt=MessageFactory.text("These are the five closest business centers. Choose a business center to learn more"),
                    retry_prompt=MessageFactory.text("Select a valid option"),
                    choices = [Choice(value=i) for i in crank(selection)],
                    style= ListStyle("5")
                )
            )
    
    async def fourth_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        selection = step_context.result.value
        region = ""
        
        if selection == "Africa Regional Expertise":
            selection = "Chicago MBDA Export Project"
            region = "Africa"
        elif selection == "Asia Regional Expertise":
            selection = "Miami MBDA Exporting Project"
            region = "Asia"
        elif selection == "Latin America Regional Expertise":
            selection = "San Antonio MBDA Export Project"
            region = "Latin America"
        elif selection == "Caribbean Expertise":
            selection = "Miami MBDA Exporting Project"
            region = "Caribbean"
        

        d = jsonlog_read()
        d['BusinessCenterSearch'].append(selection)
        jsonlog_write(d)

        with open('cards/BusinessCenterInfo2.json') as jsonfile:
            card = json.load(jsonfile)
        
        info = bcinfo(selection)

        card['body'][1]['columns'][0]['items'][0]['facts'][0]['value'] = info['Center Name']
        card['body'][1]['columns'][0]['items'][0]['facts'][1]['value'] = info['Location ']
        card['body'][1]['columns'][1]['items'][0]['facts'][0]['value'] = info['Center Address']
        card['body'][1]['columns'][1]['items'][0]['facts'][1]['value'] = info['PD\'s #']
        card['body'][4]['text'] = info['About this Center']
        card['actions'][0]['url'] = info['Annual Analysis']

        card['body'][2]['columns'][1]['items'][0]['text'] = region

        message = Activity(
            text=f"You selected {selection}",
            type=ActivityTypes.message,
            attachments=[CardFactory.adaptive_card(card)],
        )
        await step_context.context.send_activity(message)

        return await step_context.prompt(
            CARD_PROMPT,
            PromptOptions(
                prompt=MessageFactory.text("Do you want to lookup an additional business center?"),
                retry_prompt=MessageFactory.text("Select a valid option"),
                choices = [
                    Choice(value="Yes"),
                    Choice(value="No")
                ]
            )
        )
    
    async def fifth_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        selection = step_context.result.value

        if selection == "Yes": 
            return await step_context.replace_dialog(BusinessDialog.__name__)
        elif selection == "No":
            return await step_context.end_dialog("exit")
        
    