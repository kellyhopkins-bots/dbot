import json

from botbuilder.core import MessageFactory, CardFactory, UserState
from botbuilder.dialogs import (
    WaterfallDialog,
    DialogTurnResult,
    WaterfallStepContext,
    ComponentDialog,
)
from botbuilder.dialogs.prompts import PromptOptions, TextPrompt, NumberPrompt, ChoicePrompt, AttachmentPrompt
from botbuilder.dialogs.choices import Choice, ListStyle
from botbuilder.schema import ChannelAccount, CardAction, ActionTypes, SuggestedActions, Attachment, Activity, ActivityTypes
from botbuilder.ai.qna import QnAMaker, QnAMakerEndpoint

from data_models import BusinessProfile

from services import Qna, Luis, readfile, deletefile, writefile, jsonlog_read, jsonlog_write
from cards import cards


CARD_PROMPT = "cardPrompt"

class DataDialog(ComponentDialog):
    def __init__(self, dialog_id: str = None):
        super(DataDialog, self).__init__(dialog_id or DataDialog.__name__)

        # self.user_profile_accessor = user_state.create_property("UserProfile ")

        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(NumberPrompt(NumberPrompt.__name__))
        self.add_dialog(ChoicePrompt(CARD_PROMPT))
        self.add_dialog(AttachmentPrompt(AttachmentPrompt.__name__))

        self.BUSINESS_INFO = "value-userInfo"

        self.qna_maker = QnAMaker(
            QnAMakerEndpoint(
                knowledge_base_id="14a2ec31-7ba5-4ea9-b37c-7aa02c803402",
                endpoint_key="5e2bd5e5-71f9-4375-a3ab-2dbff3c81ace",
                host="https://mbda-qna-bot.azurewebsites.net/qnamaker",
            )
        )
        

        self.add_dialog(WaterfallDialog("WFDialog", [
            self.first_step,
            self.second_step,
            self.extra_step,
            self.extra_step2,
            self.third_step,
            self.fourth_step,
            self.fifth_step,
            self.sixth_step,
            self.seventh_step,
            self.eighth_step,
            self.ninth_step
            ]))

        self.initial_dialog_id = "WFDialog"



    async def first_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        # await step_context.context.send_activity(cards.displayCard("BusinessInfo"))
        # await step_context.context.send_activity(
        #     MessageFactory.text("Fill out the above form or answer these next questions.")
        # )
        return await step_context.prompt(TextPrompt.__name__, PromptOptions(
            prompt = cards.suggested("Is your business a certified MBE?", ["Yes", "No", "Not sure"])
        ))
    
    async def second_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        response = Luis(query=result)
        intent = response['topIntent']

        d = jsonlog_read()
        
        if intent == "Choice.Yes":
            d['CertifiedMBE'] = "Yes"
            jsonlog_write(d)
            return await step_context.next("")
        elif intent == "Choice.No":
            d['CertifiedMBE'] = "No"
            jsonlog_write(d)
            return await step_context.prompt(TextPrompt.__name__, PromptOptions(
                prompt= cards.suggested("Would you like information on how to become a certified MBE?", ['Yes', 'No'])
            ))
        else:
            print("...")

    async def extra_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        # intent = Luis(query=result)['topIntent']

        if result == "":
            return await step_context.next("")
        else:
            intent = Luis(query=result)['topIntent']
            if intent == "Choice.Yes":
                return await step_context.prompt(TextPrompt.__name__, PromptOptions(
                    prompt= cards.suggested("Type your question or select one of the options below", ['What are MBEs?', 'How do I become a certified MBE?'])
                ))
            else:
                return await step_context.next("")

        
    
    async def extra_step2(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        if result != "":
            intent = Luis(query=result)['topIntent']
        else:
            intent = ""

        if intent == "Qna":
            # response = await self.qna_maker.get_answers(step_context.context)
            # response = response[0].answer
            response = Qna(query=result)['answer']
            await step_context.context.send_activity(MessageFactory.text(response))
        
        return await step_context.prompt(NumberPrompt.__name__, PromptOptions(
                prompt= MessageFactory.text("How many years have you been in business?"),
                retry_prompt= MessageFactory.text("Please enter a number")
            ))

    
    async def third_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        d = jsonlog_read()
        d['YearsInBusiness'] = str(result)
        jsonlog_write(d)

        return await step_context.prompt(NumberPrompt.__name__, PromptOptions(
            prompt= MessageFactory.text("How many full time employees does your business have?"),
            retry_prompt= MessageFactory.text("Please enter a number")
        ))

    async def fourth_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        d = jsonlog_read()
        d['FullTimeEmployees'] = str(result)
        jsonlog_write(d)

        industry = ['Bio-Tech', 'Construction', 'Entertainment', 'Environmental', 'Manufacturer/Producer', 'Manufacturer - Food Products', 'Media/Publishing', 'Research & Development', "Retail", "Other", "Service", "Service - Daycare", "Service - Financial, Insurance", "Service - Food Prep, Catering, Restaurant", "Service - Healthcare", "Service - Salon, Spa", "Technology", "Wholesale/Distributor"]

        return await step_context.prompt(CARD_PROMPT, PromptOptions(
            prompt= MessageFactory.text("What industry does your business operate in?"),
            choices= [Choice(value=i) for i in industry],
            style= ListStyle("4")
        ))
    
    async def fifth_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result.value
        d = jsonlog_read()
        d['Industry'] = result
        jsonlog_write(d)

        return await step_context.prompt(TextPrompt.__name__, PromptOptions(
            prompt= cards.suggested("Does your business have high growth potential", ['Yes', 'No'])
        ))

    async def sixth_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        response = Luis(query=result)
        intent = response['topIntent']

        d = jsonlog_read()

        if intent == "Choice.Yes":
            d['HighGrowthPotential'] = "Yes"
            jsonlog_write(d)
            return await step_context.prompt(TextPrompt.__name__, PromptOptions(
                prompt= cards.suggested("How tangible is the potential", ['Engaged with VC', 'Research Grants', 'Prototype Created'])
            ))
        else:
            d['HighGrowthPotential'] = "No"
            jsonlog_write(d)
            return await step_context.next("")

    async def seventh_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        d = jsonlog_read()
        d['PotentialDetails'] = result
        jsonlog_write(d)

        with open('cards/BusinessInfo.json') as jsonfile:
            card = json.load(jsonfile)
        
        card['body'][1]['columns'][1]['items'][0]['text'] = d['CertifiedMBE']
        card['body'][2]['columns'][1]['items'][0]['text'] = d['YearsInBusiness']
        card['body'][3]['columns'][1]['items'][0]['text'] = d['FullTimeEmployees']
        card['body'][4]['columns'][1]['items'][0]['text'] = d['Industry']
        card['body'][5]['columns'][1]['items'][0]['text'] = d['HighGrowthPotential']
        

        message = Activity(
            text="Review the information you entered",
            type=ActivityTypes.message,
            attachments=[CardFactory.adaptive_card(card)],
        )
        await step_context.context.send_activity(message)

        return await step_context.prompt(TextPrompt.__name__, PromptOptions(
            prompt= cards.suggested("Look Correct?", ['Yes', 'No'])
        ))

        #return await step_context.end_dialog(d)
    
    async def eighth_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        response = Luis(query=result)
        intent = response['topIntent']

        d = jsonlog_read()
        d["VettedUser"] = "Yes"
        jsonlog_write(d)

        if intent == "Choice.Yes":
            return await step_context.end_dialog("")
        else:
            with open('cards/BusinessInfoForm.json') as jsonfile:
                card = json.load(jsonfile)
        
            card['body'][2]['value'] = d['CertifiedMBE']
            card['body'][4]['value'] = d['YearsInBusiness']
            card['body'][6]['value'] = d['FullTimeEmployees']
            card['body'][8]['value'] = d['Industry']

            industry = ['Bio-Tech', 'Construction', 'Entertainment', 'Environmental', 'Manufacturer/Producer', 'Manufacturer - Food Products', 'Media/Publishing', 'Research & Development', "Retail", "Other", "Service", "Service - Daycare", "Service - Financial, Insurance", "Service - Food Prep, Catering, Restaurant", "Service - Healthcare", "Service - Salon, Spa", "Technology", "Wholesale/Distributor"]
            card['body'][8]['choices'] = [{"title": i, "value": i} for i in industry]

            message = Activity(
                text="You can update the below information",
                type=ActivityTypes.message,
                attachments=[CardFactory.adaptive_card(card)],
            )
            # await step_context.context.send_activity(message)
            # return await step_context.prompt(AttachmentPrompt.__name__, PromptOptions(
            #     prompt= MessageFactory.attachment(CardFactory.adaptive_card(card))
            #     #prompt= message
            # ))

            return await step_context.prompt(TextPrompt.__name__, PromptOptions(
                prompt= MessageFactory.attachment(CardFactory.adaptive_card(card))
            ))
    
    async def ninth_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        data = json.loads(result)
        values = list(data.values())

        updates = ['CertifiedMBE', 'YearsInBusiness', 'FullTimeEmployees', 'Industry']

        m = dict(zip(updates, values))

        d = jsonlog_read()
        for i in updates:
            d[i] = m[i]
        jsonlog_write(d)

        with open('cards/BusinessInfo.json') as jsonfile:
            card = json.load(jsonfile)
        
        card['body'][1]['columns'][1]['items'][0]['text'] = d['CertifiedMBE']
        card['body'][2]['columns'][1]['items'][0]['text'] = d['YearsInBusiness']
        card['body'][3]['columns'][1]['items'][0]['text'] = d['FullTimeEmployees']
        card['body'][4]['columns'][1]['items'][0]['text'] = d['Industry']
        card['body'][5]['columns'][1]['items'][0]['text'] = d['HighGrowthPotential']

        

        message = Activity(
            text="Review the information you entered",
            type=ActivityTypes.message,
            attachments=[CardFactory.adaptive_card(card)],
        )
        await step_context.context.send_activity(message)

        return await step_context.end_dialog()
        # step_context.context.activity.text
        #print(result)
        #step_context.end_dialog("Hi")



    
    # async def first_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
    #     step_context.values[self.BUSINESS_INFO] = BusinessProfile()

    #     return await step_context.prompt(TextPrompt.__name__, PromptOptions(
    #         prompt=MessageFactory.text("Enter business name:")
    #     ))
    
    # async def second_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
    #     business_profile = step_context.values[self.BUSINESS_INFO]
    #     business_profile.name = step_context.result

    #     return await step_context.prompt(NumberPrompt.__name__, PromptOptions(
    #         prompt=MessageFactory.text("Enter revenue:")
    #     ))

    # async def third_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
    #     business_profile = step_context.values[self.BUSINESS_INFO]
    #     business_profile.rev = step_context.result

    #     print(step_context.values)

    #     return await step_context.end_dialog(business_profile)