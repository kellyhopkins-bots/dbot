from botbuilder.core import MessageFactory, CardFactory, UserState
from botbuilder.dialogs import (
    WaterfallDialog,
    DialogTurnResult,
    WaterfallStepContext,
    ComponentDialog,
)
from botbuilder.dialogs.prompts import PromptOptions, TextPrompt, NumberPrompt, ChoicePrompt
from botbuilder.dialogs.choices import Choice
from botbuilder.schema import ChannelAccount, CardAction, ActionTypes, SuggestedActions

from botbuilder.ai.qna import QnAMaker, QnAMakerEndpoint

from data_models import BusinessProfile

from config import DefaultConfig

from services import Qna, Luis, jsonlog_read, jsonlog_write
from cards import cards

from dialogs.question_dialog import QuestionDialog
from dialogs.business_dialog import BusinessDialog

CARD_PROMPT = "cardPrompt"

class EmergencyDialog(ComponentDialog):
    def __init__(self, dialog_id: str = None):
        super(EmergencyDialog, self).__init__(EmergencyDialog.__name__)

        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(QuestionDialog(QuestionDialog.__name__))

        self.add_dialog(WaterfallDialog("WFDialog", [self.first_step, self.second_step, self.third_step, self.fourth_step]))

        self.initial_dialog_id = "WFDialog"
    
    async def first_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        return await step_context.prompt(TextPrompt.__name__, PromptOptions(
            prompt= cards.suggested("Type your question or select an option below:", [
                "COVID-19",
                "CARES Act",
                "Dynamic Emergency Assistance"
            ])
        ))
    
    async def second_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        
        if result == "COVID-19":
            return await step_context.prompt(TextPrompt.__name__, PromptOptions(
                prompt= cards.suggested("Type your question or select a topic below:", ["Example 1", "Example 2"])
            ))
            # return await self.first_step(step_context)
        elif result == "CARES Act":
            return await step_context.prompt(TextPrompt.__name__, PromptOptions(
                prompt= cards.suggested("Type your question or select a topic below:", ["Example 1", "Example 2"])
            ))
        elif result == "Dynamic Emergency Assistance":
            return await step_context.prompt(TextPrompt.__name__, PromptOptions(
                prompt= cards.suggested("Type your question or select a topic below:", ["Example 1", "Example 2"])
            ))
        else:
            intent = Luis(query=result)['topIntent']
            if intent == "Qna":
                answer = Qna(query=result)
                await step_context.context.send_activity(MessageFactory.text(answer))
                return await step_context.next("")
    
    async def third_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        if result != "":
            intent = Luis(query=result)['topIntent']
            if intent == "Qna":
                answer = Qna(query=result)
                await step_context.context.send_activity(MessageFactory.text(answer))
        
        return await step_context.prompt(TextPrompt.__name__, PromptOptions(
            prompt= cards.suggested("Do you have another question?", ["Yes", "No"])
        ))
    
    async def fourth_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        intent = Luis(query=result)['topIntent']

        if intent == "Choice.Yes":
            return await step_context.replace_dialog(EmergencyDialog.__name__)
        else:
            return await step_context.end_dialog()