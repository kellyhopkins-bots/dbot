from botbuilder.core import MessageFactory, CardFactory, UserState
from botbuilder.dialogs import (
    WaterfallDialog,
    DialogTurnResult,
    WaterfallStepContext,
    ComponentDialog,
)
from botbuilder.dialogs.prompts import PromptOptions, TextPrompt, NumberPrompt, ChoicePrompt
from botbuilder.dialogs.choices import Choice
from botbuilder.schema import ChannelAccount, CardAction, ActionTypes, SuggestedActions

from botbuilder.ai.qna import QnAMaker, QnAMakerEndpoint

from data_models import BusinessProfile

from config import DefaultConfig

from services import Qna, Luis, jsonlog_read, jsonlog_write
from cards import cards

from dialogs.question_dialog import QuestionDialog
from dialogs.business_dialog import BusinessDialog

CARD_PROMPT = "cardPrompt"

class FinancialDialog(ComponentDialog):
    def __init__(self, user_state: UserState): #, dialog_id: str = None):
        #super(FinancialDialog, self).__init__(dialog_id or FinancialDialog.__name__)
        super(FinancialDialog, self).__init__(FinancialDialog.__name__)

        self.user_state = user_state

        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(ChoicePrompt(CARD_PROMPT))

        self.BUSINESS_INFO = "value-userInfo"

        self.config = DefaultConfig()
        
        self.add_dialog(QuestionDialog(QuestionDialog.__name__))
        self.add_dialog(BusinessDialog(BusinessDialog.__name__))

        self.add_dialog(WaterfallDialog("WFDialog", [self.first_step, self.second_step, self.third_step, self.fourth_step, self.fifth_step]))

        self.initial_dialog_id = "WFDialog"

        self.qna_maker = QnAMaker(
            QnAMakerEndpoint(
                knowledge_base_id=self.config.QNA_KNOWLEDGEBASE_ID,
                endpoint_key=self.config.QNA_ENDPOINT_KEY,
                host=self.config.QNA_ENDPOINT_HOST,
            )
        )
    
    async def first_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        d = jsonlog_read()
        if d['VettedUser'] == "Yes":
            return await step_context.next("")
        else:
            return await step_context.begin_dialog(QuestionDialog.__name__)
    
    async def second_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        # business_info: BusinessProfile = step_context.result

        # #print(self.user_state)
        # print(business_info)

        # accessor = self.user_state.create_property("BusinessProfile")
        # await accessor.set(step_context.context, business_info)

        result = step_context.result

        if result == "exit":
            return await step_context.end_dialog("exit")

        return await step_context.prompt(TextPrompt.__name__, PromptOptions(
            prompt= cards.suggested("Select an option to get started", ["General Knowledge", "Partner Referrals", "Business Center"])
        ))

    async def third_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        response = Luis(query=result)
        intent = response['topIntent']

        if result == "General Knowledge":
            return await step_context.prompt(TextPrompt.__name__, PromptOptions(
                prompt= cards.suggested("Type your question or select an option below:", ["Contracts", "Exports"])
            ))
        elif result == "Partner Referrals":
            return await step_context.prompt(TextPrompt.__name__, PromptOptions(
                prompt= cards.suggested("Type your question or select an option below:", ["Contracts", "Exports"])
            ))
        elif result == "Business Center":
            # return await step_context.prompt(TextPrompt.__name__, PromptOptions(
            #     prompt= cards.suggested("Would you like to find a business center where you can get guidance from an expert?", ["Yes", "No"])
            # ))
            return await step_context.begin_dialog(BusinessDialog.__name__)
        else:
            response = Luis(query=result)
            intent = response['topIntent']

            d = jsonlog_read()
            d['QnaMakerQuestions'].append(step_context.result)
            jsonlog_write(d)

            if intent == "Qna":
                response = await self.qna_maker.get_answers(step_context.context)
                result = response[0].answer
            
            await step_context.context.send_activity(MessageFactory.text(response))

            return await step_context.prompt(TextPrompt.__name__, PromptOptions(
                prompt=cards.suggested("Do you have another question?", ["Yes", "No"])
            ))
    
    async def fourth_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        intent = Luis(query=result)['topIntent']

        if intent == "Qna":
            # response = await self.qna_maker.get_answers(step_context.result)
            # result = response[0].answer
            result = Qna(query=result)['answer']
            await step_context.context.send_activity(MessageFactory.text(result))
            return await step_context.prompt(TextPrompt.__name__, PromptOptions(
                prompt= cards.suggested("Do you have another question?", ["Yes", "Main Menu"])
            ))
        elif intent == "Choice.Yes":
            return await step_context.replace_dialog(FinancialDialog.__name__)
        elif intent == "Choice.No":
            return await step_context.end_dialog("exit")
        elif intent == "Choice.Cancel":
            return await step_context.end_dialog("exit")
        else:
            # response = await self.qna_maker.get_answers(step_context.result)
            # result = response[0].answer
            result = Qna(query=result)['answer']
            await step_context.context.send_activity(MessageFactory.text(result))
            return await step_context.prompt(TextPrompt.__name__, PromptOptions(
                prompt= cards.suggested("Do you have another question?", ["Yes", "Main Menu"])
            ))
    
    async def fifth_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        intent = Luis(query=result)['topIntent']

        if intent == "Choice.Yes":
            return await step_context.replace_dialog(FinancialDialog.__name__)
        elif intent == "Choice.No":
            return await step_context.end_dialog("exit")
        elif intent == "Choice.Cancel":
            return await step_context.end_dialog("exit")
        else:
            return await step_context.end_dialog()



        # if result == "exit":
        #     return await step_context.end_dialog("exit")

        # return await step_context.end_dialog()
