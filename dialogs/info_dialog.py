import os
from botbuilder.core import MessageFactory, CardFactory, ActivityHandler
from botbuilder.dialogs import (
    WaterfallDialog,
    DialogTurnResult,
    WaterfallStepContext,
    ComponentDialog,
)
from botbuilder.dialogs.prompts import PromptOptions, TextPrompt, NumberPrompt, ChoicePrompt
from botbuilder.dialogs.choices import Choice
from botbuilder.schema import ChannelAccount, CardAction, ActionTypes, SuggestedActions

from botbuilder.ai.qna import QnAMaker, QnAMakerEndpoint

from config import DefaultConfig

from services import Luis, writefile, readfile, deletefile, jsonlog_read, jsonlog_write
from cards import cards


CARD_PROMPT = "cardPrompt"


class InfoDialog(ComponentDialog):
    def __init__(self, dialog_id: str = None):
        super(InfoDialog, self).__init__(dialog_id or InfoDialog.__name__)

        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(ChoicePrompt(CARD_PROMPT))

        self.add_dialog(WaterfallDialog("WFDialog", [self.first_step, self.second_step, self.third_step, self.fourth_step]))

        self.initial_dialog_id = "WFDialog"

        self.config = DefaultConfig()

        self.qna_maker = QnAMaker(
            QnAMakerEndpoint(
                knowledge_base_id=self.config.QNA_KNOWLEDGEBASE_ID,
                endpoint_key=self.config.QNA_ENDPOINT_KEY,
                host=self.config.QNA_ENDPOINT_HOST,
            )
        )
    
    async def first_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        if os.path.exists("data/text.txt"):
            stored = readfile()
            return await step_context.next(stored)
        else:
            return await step_context.prompt(TextPrompt.__name__, PromptOptions(
            prompt= cards.suggested("Ask a question or select one of the topics below:", [
                "MBDA",
                "MBEs",
                # "MBDA Readiness",
                # "General Topics"
                "MBDA Programs"
            ])
        ))
    
    async def second_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result

        if result == "MBDA":
            return await step_context.prompt(TextPrompt.__name__, PromptOptions(
                prompt = cards.suggested("Ask a question or select one of the topics below:", [
                    "What is MBDA?",
                    "Who does MBDA serve?",
                    "What is MBDA's objective?"
                ])
            ))
        elif result == "MBEs":
            return await step_context.prompt(TextPrompt.__name__, PromptOptions(
                prompt= cards.suggested("Ask a question or select one of the topics below:", [
                    "What are MBEs?",
                    "What qualifies and a Minority Business Enterprise?",
                    "What MBDA resources are available to MBEs?"
                ])
            ))
        # elif result == "General Topics":
        #     return await step_context.prompt(TextPrompt.__name__, PromptOptions(
        #         prompt= cards.suggested("Ask a question or select one of the topics below:", [
        #             "MBDA Programs"
        #         ])
        #     ))
        else:
            response = Luis(query=result)
            intent = response['topIntent']

            d = jsonlog_read()
            d['QnaMakerQuestions'].append(step_context.result)
            jsonlog_write(d)

            if intent == "Qna":
                response = await self.qna_maker.get_answers(step_context.context)
                result = response[0].answer

                writefile(result)
                return await step_context.next(result)
            else:
                return await step_context.replace_dialog(InfoDialog.__name__)
    
    async def third_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        if os.path.exists("data/text.txt"):
            await step_context.context.send_activity(MessageFactory.text(readfile()))
        else:
            result = step_context.result

            d = jsonlog_read()
            d['QnaMakerQuestions'].append(result)
            jsonlog_write(d)

            response = await self.qna_maker.get_answers(step_context.context)
            response = response[0].answer

            # d = jsonlog_read()
            # d['QnaMakerAnswers'].append(response)
            # jsonlog_write(d)

            await step_context.context.send_activity(MessageFactory.text(response))
        
        deletefile()

        # return await step_context.prompt(TextPrompt.__name__, PromptOptions(
        #     prompt=cards.suggested("Do you have another question?", ["Yes", "No"])
        # ))

        return await step_context.prompt(TextPrompt.__name__, PromptOptions(
            prompt=cards.suggested("Do you have another question about MBDA's mission, who MBDA serves, or what programs MBDA offers?",[
                "Yes",
                "Main Menu"
            ])
        ))
    
    async def fourth_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        intent = Luis(query=result)['topIntent']

        if intent == "Choice.Yes":
            return await step_context.replace_dialog(InfoDialog.__name__)
        elif intent == "Choice.Cancel":
            return await step_context.end_dialog("exit")
        elif intent == "Choice.No":
            return await step_context.end_dialog("exit")
        else:
            return await step_context.end_dialog("exit")