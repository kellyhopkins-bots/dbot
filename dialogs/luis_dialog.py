import os
from botbuilder.core import MessageFactory, UserState
from botbuilder.dialogs import (
    WaterfallDialog,
    DialogTurnResult,
    WaterfallStepContext,
    ComponentDialog,
)
from botbuilder.dialogs.prompts import PromptOptions, TextPrompt, NumberPrompt, ChoicePrompt
from botbuilder.dialogs.choices import Choice
from botbuilder.schema import ChannelAccount, CardAction, ActionTypes, SuggestedActions

from data_models import UserProfile
from dialogs.info_dialog import InfoDialog
from dialogs.business_dialog import BusinessDialog
from dialogs.data_dialog import DataDialog
from dialogs.financial_dialog import FinancialDialog
from dialogs.strategic_dialog import StrategicDialog
from dialogs.emergency_dialog import EmergencyDialog
from dialogs.test_dialog import TestDialog

from services import Qna, Luis, readfile, deletefile, writefile, jsonlog_read, jsonlog_write
from cards import cards

CARD_PROMPT = "cardPrompt"

class LuisDialog(ComponentDialog):
    def __init__(self, user_state: UserState, dialog_id: str = None):
        super(LuisDialog, self).__init__(dialog_id or LuisDialog.__name__)

        self.user_state = user_state

        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(NumberPrompt(NumberPrompt.__name__))
        self.add_dialog(ChoicePrompt(CARD_PROMPT))

        self.add_dialog(InfoDialog(InfoDialog.__name__))
        self.add_dialog(BusinessDialog(BusinessDialog.__name__))
        self.add_dialog(DataDialog(DataDialog.__name__))
        self.add_dialog(FinancialDialog(FinancialDialog.__name__))
        self.add_dialog(StrategicDialog(StrategicDialog.__name__))
        self.add_dialog(EmergencyDialog(EmergencyDialog.__name__))
        self.add_dialog(TestDialog(TestDialog.__name__))

        self.add_dialog(
            WaterfallDialog(
                "WFDialog",
                [
                    self.luis_step,
                    self.intent_step,
                    self.last_step
                ],
            )
        )

        self.initial_dialog_id = "WFDialog"

    async def luis_step(self, step_context: WaterfallStepContext) -> DialogTurnResult: 

        if os.path.exists('data/text.txt'):
            f = readfile()
            deletefile()
            return await step_context.next(f)
        else:
            return await step_context.prompt(TextPrompt.__name__, PromptOptions(
                prompt = cards.suggested("Select an option below:", [
                    "General Information",
                    "Financial Resources",
                    "Strategic Assistance",
                    "Business Center Experts",
                    "Emergency Assistance",
                    "Quit",
                    # "Test Dialog"
                ])
            ))


    async def intent_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        try:
            request = step_context.result.value
        except:
            request = step_context.result
        
        response = Luis(query=request)
        intent = response['topIntent']

        await step_context.context.send_activity(
            MessageFactory.text(f"Luis intent: {intent}")
        )

        d = jsonlog_read()
        d['usecases'].append(intent)
        jsonlog_write(d)
        
        if intent == "Weather.CheckWeatherValue":
            message = "TODO: weather flow here"
        elif intent == "GeneralInformation":
            return await step_context.begin_dialog(InfoDialog.__name__)
        elif intent == "BusinessCenter":
            return await step_context.begin_dialog(BusinessDialog.__name__)
        elif intent == "FinancialResources":
            return await step_context.begin_dialog(FinancialDialog.__name__)
        elif intent == "StrategicAssistance":
            return await step_context.begin_dialog(StrategicDialog.__name__)
        elif intent == "EmergencyAssistance":
            return await step_context.begin_dialog(EmergencyDialog.__name__)
        elif intent == "Qna":
            writefile(request)
            return await step_context.begin_dialog(InfoDialog.__name__)
        elif intent == "Choice.Cancel":
            return await step_context.end_dialog("quit")
        else:
            message = "Unknown Intent"
            return await step_context.replace_dialog(LuisDialog.__name__)
            #return await step_context.begin_dialog(TestDialog.__name__)

        
    async def last_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        message = "End of dialog."

        await step_context.context.send_activity(
            MessageFactory.text(message)
        )

        if step_context.result == "exit":
            return await step_context.end_dialog("exit")

        return await step_context.end_dialog()  
    

    def get_choices(self):
        card_options = [
            Choice(value="General Information", synonyms=["1"]),
            Choice(value="Financial Resources", synonyms=["2"]),
            Choice(value="Strategic Assistance", synonyms=["3"]),
            Choice(value="Business Center Experts", synonyms=["4"]),
            Choice(value="Emergency Assistance", synonyms=["5"]),
        ]

        return card_options