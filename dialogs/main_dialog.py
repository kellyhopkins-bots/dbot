# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

from botbuilder.dialogs import (
    ComponentDialog,
    WaterfallDialog,
    WaterfallStepContext,
    DialogTurnResult,
)
from botbuilder.core import MessageFactory, UserState

from botbuilder.dialogs.prompts import PromptOptions, TextPrompt, NumberPrompt, ChoicePrompt



from data_models import UserProfile, UserDetails
from dialogs.top_level_dialog import TopLevelDialog
from dialogs.custom_dialog import CustomDialog
from dialogs.luis_dialog import LuisDialog
from dialogs.suggestion_dialog import SuggestionDialog

from services import Qna, Luis, writefile, deletefile
from cards import cards


class MainDialog(ComponentDialog):
    def __init__(self, user_state: UserState):
        super(MainDialog, self).__init__(MainDialog.__name__)

        self.user_state = user_state

        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(NumberPrompt(NumberPrompt.__name__))
        #self.add_dialog(ChoicePrompt(CARD_PROMPT))

        self.add_dialog(TopLevelDialog(TopLevelDialog.__name__))
        self.add_dialog(CustomDialog(CustomDialog.__name__))
        self.add_dialog(LuisDialog(LuisDialog.__name__))
        self.add_dialog(SuggestionDialog(SuggestionDialog.__name__))
        self.add_dialog(
            WaterfallDialog("WFDialog", [self.initial_step, self.ask_step, self.luis_step, self.last_step])
        )

        self.initial_dialog_id = "WFDialog"

    async def initial_step(
        self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:
        deletefile()
        return await step_context.begin_dialog(LuisDialog.__name__)

    async def ask_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        if step_context.result == "exit":
            return await step_context.replace_dialog(MainDialog.__name__)
        elif step_context.result == "quit":
            return await step_context.next("No")
        else:
            return await step_context.prompt(TextPrompt.__name__, PromptOptions(
                prompt = cards.suggested("Do you have another question?", ["Yes", "No"])
            ))

    
    async def luis_step(self, step_context:WaterfallStepContext) -> DialogTurnResult:
        request = step_context.result
        response = Luis(query=request)
        intent = response['topIntent']

        if intent == "Choice.Yes":
            return await step_context.replace_dialog(MainDialog.__name__)
        elif intent == "Choice.No":
            await step_context.context.send_activity(
                MessageFactory.text("Glad I was able to help!")
            )
            return await step_context.continue_dialog()
        else:
            writefile(request)
            return await step_context.replace_dialog(MainDialog.__name__)
        
    
    async def last_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        #user_info: UserDetails = step_context.result

        return await step_context.begin_dialog(SuggestionDialog.__name__)
