from botbuilder.core import MessageFactory, CardFactory, UserState
from botbuilder.dialogs import (
    WaterfallDialog,
    DialogTurnResult,
    WaterfallStepContext,
    ComponentDialog,
)
from botbuilder.dialogs.prompts import PromptOptions, TextPrompt, NumberPrompt, ChoicePrompt
from botbuilder.dialogs.choices import Choice
from botbuilder.schema import ChannelAccount, CardAction, ActionTypes, SuggestedActions

from data_models import BusinessProfile

from services import Qna, Luis
from cards import cards

from dialogs.data_dialog import DataDialog

CARD_PROMPT = "cardPrompt"

class QuestionDialog(ComponentDialog):
    def __init__(self, user_state: UserState): #, dialog_id: str = None):
        #super(FinancialDialog, self).__init__(dialog_id or FinancialDialog.__name__)
        super(QuestionDialog, self).__init__(QuestionDialog.__name__)

        self.user_state = user_state

        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(ChoicePrompt(CARD_PROMPT))

        self.BUSINESS_INFO = "value-userInfo"
        
        self.add_dialog(DataDialog(DataDialog.__name__))

        self.add_dialog(WaterfallDialog("WFDialog", [self.first_step, self.second_step, self.third_step, self.fourth_step]))

        self.initial_dialog_id = "WFDialog"
    
    async def first_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        await step_context.context.send_activity(MessageFactory.text(
            "In order to continue this conversation, I need to ask you some questions about your business"
        ))
        return await step_context.prompt(TextPrompt.__name__, PromptOptions(
            prompt= cards.suggested("Are you comfortable continuing?", ['Yes', 'No'])
        ))
    
    async def second_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        request = step_context.result
        response = Luis(query=request)
        intent = response['topIntent']

        if intent == "Choice.Yes":
            return await step_context.next("Yes")
        else:
            await step_context.context.send_activity(MessageFactory.text(
                "Unfortunately, I am unable to address your specific questions if you don't provide this information."
            ))
            return await step_context.prompt(TextPrompt.__name__, PromptOptions(
                prompt=cards.suggested("Would you like to continue?", ['Yes', 'No'])
            ))
    
    async def third_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        response = Luis(query=result)
        intent = response['topIntent']

        if result == "Yes" or intent == "Choice.Yes":
            return await step_context.next("")
        else:
            await step_context.context.send_activity(MessageFactory.text(
                "Ok, I am unable to assist you with this request then. Returning you to the main menu..."
            ))
            return await step_context.end_dialog("exit")
    
    async def fourth_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        return await step_context.begin_dialog(DataDialog.__name__)

