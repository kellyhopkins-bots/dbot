from botbuilder.core import MessageFactory, CardFactory, UserState
from botbuilder.dialogs import (
    WaterfallDialog,
    DialogTurnResult,
    WaterfallStepContext,
    ComponentDialog,
)
from botbuilder.dialogs.prompts import PromptOptions, TextPrompt, NumberPrompt, ChoicePrompt
from botbuilder.dialogs.choices import Choice
from botbuilder.schema import ChannelAccount, CardAction, ActionTypes, SuggestedActions

from botbuilder.ai.qna import QnAMaker, QnAMakerEndpoint

from data_models import BusinessProfile

from config import DefaultConfig

from services import Qna, Luis, jsonlog_read, jsonlog_write
from cards import cards

from dialogs.question_dialog import QuestionDialog
from dialogs.business_dialog import BusinessDialog

CARD_PROMPT = "cardPrompt"

class StrategicDialog(ComponentDialog):
    def __init__(self, dialog_id: str = None):
        super(StrategicDialog, self).__init__(StrategicDialog.__name__)

        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(QuestionDialog(QuestionDialog.__name__))
        self.add_dialog(BusinessDialog(BusinessDialog.__name__))

        self.add_dialog(WaterfallDialog("WFDialog", [self.first_step, self.second_step, self.third_step, self.fourth_step]))

        self.initial_dialog_id = "WFDialog"
    
    async def first_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        d = jsonlog_read()
        if d['VettedUser'] == "Yes":
            return await step_context.next("")
        else:
            return await step_context.begin_dialog(QuestionDialog.__name__)
    
    async def second_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        return await step_context.prompt(TextPrompt.__name__, PromptOptions(
            prompt= cards.suggested("Strategic Assistance options:", [
                "New Market Guidance",
                "Contracts",
                "Global Markets/Exports"
            ])
        ))

    async def third_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result

        d = jsonlog_read()
        d['StrategicOption'] = result
        jsonlog_write(d)

        return await step_context.prompt(TextPrompt.__name__, PromptOptions(
            prompt= cards.suggested(f"Select a {result} option:", ["General FAQ", "Partner Referral", "Business Center Expert"])
        ))
    
    async def fourth_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result

        if result == "Business Center Expert":
            return await step_context.begin_dialog(BusinessDialog.__name__)

        d = jsonlog_read()
        d['StrategicTier'] = result
        jsonlog_write(d)

        questions = {
            "New Market Guidance": {
                "General FAQ": ["Question 1", "Question 2", "Question 3"],
                "Partner Referral": ["Question 4", "Question 5", "Question 6"]
            },
            "Contracts": {
                "General FAQ": ["Question 7", "Question 8", "Question 9"],
                "Partner Referral": ["Question 10", "Question 11", "Question 12"]
            },
            "Global Markets/Exports": {
                "General FAQ": ["Question 13", "Question 14", "Question 15"],
                "Partner Referral": ["Question 16", "Question 17", "Question 18"]
            }
        }

        return await step_context.prompt(TextPrompt.__name__, PromptOptions(
            prompt= cards.suggested("Select an option or type your question below:", questions[d['StrategicOption']][d['StrategicTier']])
        ))
    
    async def fifth_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        # intent = Luis(query=result)['topIntent']
        answer = Qna(query=result)['answer']
        print(answer)

        await step_context.context.send_activity(MessageFactory.text(answer))
        return await step_context.prompt(TextPrompt.__name__, PromptOptions(
            prompt= cards.suggested("Do you want to ask another strategic assistance question?", ["Yes", "No"])
        ))
    
    async def sixth_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        intent = Luis(query=result)['topIntent']

        if intent == "Choice.Yes":
            return await step_context.replace_dialog(StrategicDialog.__name__)
        else:
            return await step_context.end_dialog("")