from botbuilder.core import MessageFactory, CardFactory, UserState
from botbuilder.dialogs import (
    WaterfallDialog,
    DialogTurnResult,
    WaterfallStepContext,
    ComponentDialog,
)
from botbuilder.dialogs.prompts import PromptOptions, TextPrompt, NumberPrompt, ChoicePrompt
from botbuilder.dialogs.choices import Choice
from botbuilder.schema import ChannelAccount, CardAction, ActionTypes, SuggestedActions

from botbuilder.ai.qna import QnAMaker, QnAMakerEndpoint

from data_models import BusinessProfile

from config import DefaultConfig

from services import Qna, Luis, jsonlog_read, jsonlog_write
from cards import cards

from dialogs.question_dialog import QuestionDialog
from dialogs.business_dialog import BusinessDialog

CARD_PROMPT = "cardPrompt"

class TestDialog(ComponentDialog):
    def __init__(self, dialog_id: str = None):
        super(TestDialog, self).__init__(dialog_id or TestDialog.__name__)

        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(ChoicePrompt(CARD_PROMPT))
        

        self.add_dialog(WaterfallDialog("WFDialog", [self.first_step, self.second_step]))

        self.initial_dialog_id = "WFDialog"
    
    async def first_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        return await step_context.prompt(TextPrompt.__name__, PromptOptions(
            prompt= cards.suggested("Ask a question or select an option below:", ["Sample Question"])
        ))
    
    async def second_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result
        response = Qna(query=result)
        answer = response['answer']
        try:
            prompts = response['context']['prompts']
            return await step_context.prompt(TextPrompt.__name__, PromptOptions(
                prompt= cards.suggested(answer, [i['displayText'] for i in prompts])
            ))
        except:
            print(...)

        
    async def third_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        result = step_context.result