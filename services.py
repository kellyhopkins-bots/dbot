import os
import numpy as np
import pandas as pd

from config import DefaultConfig

import re

from geopy import distance

from botbuilder.core import MessageFactory, CardFactory
from botbuilder.schema import ChannelAccount, CardAction, ActionTypes, SuggestedActions, Attachment, Activity, ActivityTypes

from uuid import uuid4 as uid
import time
import datetime

import requests
import certifi
import urllib3
import json
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

session = requests.Session()
session.verify = False

query = "Hi"

# luis = "https://eastus.api.cognitive.microsoft.com/luis/prediction/v3.0/apps/ab744ed0-1d87-4160-ad36-744b3cea5396/slots/production/predict?subscription-key=8a5b07dc63a84a35ae847a81d69195c1&verbose=true&show-all-intents=true&log=true&query="

# qna = "https://mbda-qna-bot.azurewebsites.net/qnamaker/knowledgebases/14a2ec31-7ba5-4ea9-b37c-7aa02c803402/generateAnswer"

c = DefaultConfig()
qna = c.QNA_ENDPOINT_HOST + "/knowledgebases/" + c.QNA_KNOWLEDGEBASE_ID + "/generateAnswer"
luis = f"https://westus.api.cognitive.microsoft.com/luis/prediction/v3.0/apps/{c.LUIS_APP_ID}/slots/production/predict?subscription-key={c.LUIS_API_KEY}&verbose=true&show-all-intents=true&log=true&query="
headers = {
    "Authorization": c.QNA_ENDPOINT_KEY, 
    "content-type":"application/json",
    'Accept': 'application/json, text/plain, */*',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'en-US,en;q=0.9,hi;q=0.8',
    'Connection': 'keep-alive'
    }
payload = {"question": query}

#verify = certifi.where()
#qna_request = session.post(qna, data=json.dumps(payload), headers=headers).json()
#print(payload)
#luis_request = requests.get(luis + query).json()

#print(qna_request)

def Qna(qna=qna, headers=headers, query=query, ses=session):
    payload = {"question": query}
    res = ses.post(qna, data=json.dumps(payload), headers=headers).json()
    return res['answers'][0]

def Luis(luis=luis, query=query, ses=session):
    res = ses.get(luis + query).json()
    return res['prediction']


#res["answers"][0]['answer']

#res['prediction']['topIntent']
#pred = res['prediction']
#pred['topIntent']
#pred['entities']

#prompts = [i['displayText'] for i in res['context']['prompts']]

#date=ent['datetimeV2'][0]['values'][0]['timex']

def bclookup(search):
    data = pd.read_csv('data/business_center.csv')

    regex = "^[0-9]{5}$"
    match = re.findall(regex, search)
    if len(match) > 0:
        zip = match[0]
        results = data[data['Zip'] == zip]['Center Name']
        return list(results)
    else:
        domain = search
        return list(data[data['Domain'] == domain]['Center Name']) 

def bcinfo(search):
    data = pd.read_csv('data/bc.csv')
    #text = data[data["Center Name"] == search].reset_index()["About this Center"][0]
    return data[data['Center Name'] == search].iloc[0].to_dict()

def ziplookup(zip):
    data = pd.read_csv('data/business_center.csv')

    results = data[data['Zip'] == zip]['Center Name']

    return list(results)

def clookup(zip):
    #data = pd.read_csv('data/bc.csv')

    # api = f"https://public.opendatasoft.com/api/records/1.0/search/?dataset=us-zip-code-latitude-and-longitude&q={zip}&facet=state&facet=timezone&facet=dst"
    # results = session.get(api).json()
    # coords = results['records'][0]['geometry']['coordinates']
    # coords = tuple(coords)

    data = pd.read_csv(r"data\us-zip-code-latitude-and-longitude.csv", delimiter=";")
    data['Zip'] = data['Zip'].astype(str)
    data['Zip'] = data['Zip'].str.pad(width=5, side='left', fillchar='0')
    c = data[data['Zip'] == zip]['geopoint'].values[0]
    return eval(c)


    #return coords

# def crank(zip):
#     data = pd.read_csv('data/bc.csv')
#     target = clookup(zip)
#     target = tuple(reversed(target))
#     coords = list(data['Coordinates'])
#     dist = []
#     for c in coords:
#         x = eval(c)
#         x.reverse()
#         d = distance.distance(target, tuple(x)).miles
#         dist.append(d)
#     data['distance'] = dist

#     results = data[['Center Name', 'distance']].sort_values(by='distance', ascending=True).head(5)
#     #return results
#     return list(results['Center Name'])

def crank(zip):
    data = pd.read_csv('data/bc.csv')
    target = clookup(zip)
    coords = list(data['Coordinates'])
    c = [tuple(reversed(tuple(eval(c)))) for c in coords]
    d = [distance.distance(target, x).miles for x in c]
    data['distance'] = d
    results = data[['Center Name', 'distance']].sort_values(by='distance', ascending=True).head(5)
    results.to_csv('data/test.csv')
    return list(results['Center Name'])


def crank2(zip):
    data = pd.read_csv('data/bc.csv')
    target = clookup(zip)
    target = tuple(reversed(target))
    coords = list(data['Coordinates'])
    dist = []
    for c in coords:
        x = eval(c)
        x.reverse()
        d = distance.distance(target, tuple(x)).miles
        dist.append(d)
    data['distance'] = dist

    results = data[['Center Name', 'distance']].sort_values(by='distance', ascending=True).head(5)
    return results
    #return list(results['Center Name'])

def crank3(zip):
    data = pd.read_csv('data/bc.csv')
    target = clookup(zip)
    #target = tuple(reversed(target))
    coords = list(data['Coordinates'])
    # dist = []
    # for c in coords:
    #     x = eval(c)
    #     x.reverse()
    #     d = distance.distance(target, tuple(x)).miles
    #     dist.append(d)

    c = [tuple(reversed(tuple(eval(c)))) for c in coords]
    d = [distance.distance(target, x).miles for x in c]

    data['distance'] = d

    results = data[['Center Name', 'distance']].sort_values(by='distance', ascending=True).head(5)
    results.to_csv('data/test.csv')
    return results
    #return list(results['Center Name'])

def writefile(text, file="data/text.txt"):
    tf = open(file, "w+")
    n = tf.write(text)
    tf.close

def deletefile(file="data/text.txt"):
    if os.path.exists(file):
        os.remove(file)
        return True
    else:
        return False

def readfile(file="data/text.txt"):
    with open(file) as f:
        data = f.read()
        return data

def displayCard(card):
    with open(f"cards/{card}.json") as jsonfile:
        data = json.load(jsonfile)
    
    return Activity(
                    #text=f"You selected {selection}",
                    type=ActivityTypes.message,
                    attachments=[CardFactory.adaptive_card(data)],
                )


def jsonlog_init():

    mydir = "data/json/tmp"
    list( map( os.unlink, (os.path.join( mydir,f) for f in os.listdir(mydir)) ) )

    path = "data/json/template.json"
    with open(path) as jsonfile:
        data = json.load(jsonfile)
        data['id'] = str(uid())[0:6]
        data['timestamp'] = str(datetime.datetime.utcnow())
        
        with open(f"data/json/tmp/{data['id']}.json", "w+") as jfile:
            json.dump(data, jfile)
        
        writefile(data['id'], file="data/json/tmp/tmp.txt")

def jsonlog_read():
    id = readfile(file="data/json/tmp/tmp.txt")
    x = readfile(file=f"data/json/tmp/{id}.json")
    return json.loads(x)

def jsonlog_write(d):
    id = d['id']

    with open(f"data/json/tmp/{id}.json", "w+") as jfile:
        json.dump(d, jfile)

def jsonlog_item(t):
    d = jsonlog_read()
    eval(t)
    jsonlog_write(d)


#bgh